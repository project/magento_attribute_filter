<?php
// $Id$

/**
 * @file
 * magento_attribute_filter.settings.inc
 */

/**
 * Menu callback for administration page
 */
function magento_attribute_filter_settings() {
  $form = array();

  $form['magento_attribute_filter_blacklist'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Blacklist'),
    '#collapsible' => FALSE,
  );

  $form['magento_attribute_filter_blacklist']['magento_attribute_filter_blacklist_attributes'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Attributes'),
    '#description'   => t('Put all blacklisted attributes, one per line'),
    '#default_value' => variable_get('magento_attribute_filter_blacklist_attributes', '')
  );

  $form = system_settings_form($form);
  return $form;
}
